var video = document.getElementById("video"),
	wrap = document.getElementById("wrap"),
	playButton = document.getElementById("play"),
	mute = document.getElementById("mute");

video.addEventListener("waiting", function(){ wrap.classList.add('buffering'); });
video.addEventListener("playing", function(){ wrap.classList.remove('buffering'); });

// Event listener for the play/pause button
video.addEventListener("click", function() {
	video.pause();
	playButton.classList.add('paused');
});	
playButton.addEventListener("click", function() {

	if (video.paused === true) {
		// Play the video
		video.play();
		playButton.classList.remove('paused');
	} else {
		// Pause the video
		video.pause();
		playButton.classList.add('paused');
	}
});				

mute.addEventListener('click',function(){
	if(mute.classList.contains('muted')){
		mute.classList.remove('muted');
		video.muted = false;
	}else{
		mute.classList.add('muted');	
		video.muted = true;		
	}
});

video.addEventListener('ended',function(){
    video.load();
    video.pause();   
    playButton.classList.add('paused');
},false);	


function resizeVideo(){
	//console.log(video.readyState);
	var vr = video.videoWidth/video.videoHeight;
	var wr = window.innerWidth/window.innerHeight;
	if(vr > wr){
		//console.log('tall');
		var rh = window.innerHeight*vr;
		var l = -(rh - window.innerWidth)*0.5;
		video.style.height = Math.floor(window.innerHeight) + "px";
		video.style.width = Math.floor(rh) + "px";
		video.style.left = l + "px";
		video.style.top = "0px";
	}else{
		//console.log('wide');
		var rw = window.innerWidth/vr;
		var t = -(rw - window.innerHeight)*0.5;
		video.style.width = Math.floor(window.innerWidth) + "px";
		video.style.height = Math.floor(rw) + "px";
		video.style.top = t + "px";
		video.style.left = "0px";		
	}
}
video.addEventListener("loadedmetadata", resizeVideo);
window.addEventListener('resize', resizeVideo);

if ( video.poster ) {
    var image = new Image();
    image.onload = function () {
        //video.style.opacity = 1;
        video.className += " loaded";
    };
    image.src = video.poster;
}
// video.addEventListener("canplay", function(){ 
// 	//video.play();
// 	// setTimeout(function(){
// 	// 	video.pause(); //block play so it buffers before playing
// 	// }, 1);
// 	console.log('ready');
// });

// The basic check
// if(video.readyState === 4){
// 	//console.log('ready 1');
// 	setTimeout(function(){
// 		video.pause();
// 		wrap.classList.remove('buffering');
// 	}, 1);
// }


// var interval = setInterval(function() {
//     if(video.readyState === 4){
//     	//console.log('ready 2');
//     	setTimeout(function(){
// 			video.pause();
// 			wrap.classList.remove('buffering');
// 		}, 1);
//         clearInterval(interval);
//     }
//     //console.log(video.readyState);    
// }, 100);

